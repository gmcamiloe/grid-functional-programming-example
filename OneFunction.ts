/**
 * @param currentColumn increases moves for each column in the grid.
 * @param currenRow increses for when currentColumn is greather than final.
 * @param final is the nxn limit, for example, 8 prints a 8x8 grid
 * @returns all the grid as a string
 */

function getAllGridAsString(
  currentColumn: number,
  currenRow: number,
  final: number
): string {
  if (currentColumn > final && currenRow > final) return '';
  if (currentColumn > final && currenRow <= final)
    return `\n${getAllGridAsString(1, currenRow + 1, final)}`;
  let row = '';
  if (
    (currentColumn % 2 === 0 && currenRow % 2 === 1) ||
    (currentColumn % 2 === 1 && currenRow % 2 === 0)
  )
    row += `#${getAllGridAsString(currentColumn + 1, currenRow, final)}`;
  if (
    (currentColumn % 2 === 0 && currenRow % 2 === 0) ||
    (currentColumn % 2 === 1 && currenRow % 2 === 1)
  )
    row += ` ${getAllGridAsString(currentColumn + 1, currenRow, final)}`;

  return row;
}
const grid = getAllGridAsString(1, 1, 200);
console.log(grid);
