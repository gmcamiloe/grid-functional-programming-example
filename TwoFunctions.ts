/**
 * @param currentColumn increases moves for each column in the grid.
 * @param currenRow only is uses to determine the character and stop when is greather than final
 * @param final is the nxn limit, for example, 8 prints a 8x8 grid
 * @returns a Row of the grid
 */
function getRowStr(
  currentColumn: number,
  currentRow: number,
  final: number
): string {
  if (currentColumn > final) return '';
  if (currentColumn > final && currentRow <= final)
    return `\n${getRowStr(1, currentRow + 1, final)}`;
  let row = '';
  if (
    (currentColumn % 2 === 0 && currentRow % 2 === 1) ||
    (currentColumn % 2 === 1 && currentRow % 2 === 0)
  )
    row += `#${getRowStr(currentColumn + 1, currentRow, final)}`;
  if (
    (currentColumn % 2 === 0 && currentRow % 2 === 0) ||
    (currentColumn % 2 === 1 && currentRow % 2 === 1)
  )
    row += ` ${getRowStr(currentColumn + 1, currentRow, final)}`;

  return row;
}

/**
 * @param currentRow for each row, increases if It is less than final.
 * @param final is the limit of rows, and also is the limit of columns.
 */
function printGridByRows(currentRow: number, final: number): void {
  if (currentRow > final) return;
  const newRow = getRowStr(1, currentRow, final);
  console.log(newRow);
  if (currentRow + 1 <= final) printGridByRows(currentRow + 1, final);
}

printGridByRows(1, 200);
